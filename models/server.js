const express = require('express');
const xmlparser = require('express-xml-bodyparser');
const cors = require('cors');

class Server {

    constructor() {
        this.app = express();
        this.server = require('http').createServer(this.app);
        this.io = require('socket.io')(this.server, {
            cors: {
                origin: '*',
            }
        })

        this.funciones = '/funciones'
        
        this.middlewares();
        this.routes();

    }

    middlewares() {
        this.app.use(cors());
        this.app.use(xmlparser());
        this.app.use(express.json());
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use('/', require('../routes/inicio'));
        this.app.use(this.funciones, require('../routes/funciones'));

        //funcion demo de tipo post
        this.app.post('/postdummy/:count', (req, resp)=>{
            let {count} = req.params;
            resp.json({
                valLeido:count
            })
        })

        this.app.post('/paroimpar', (req, res)=>{
            if(req.body['numero'] % 2 == 0){
                res.send("Par");
            }else{
                res.send("Impar");
            }
        })

        this.app.post('/fibo', (req, res)=>{
            const { number } = req.body;
            var fibonacci = [];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            for (var i = 2; i   < number; i++) {
                fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
            }
            res.json({
                NuneriFibonnacci: fibonacci[fibonacci.length-1]
            });
        })

        this.app.post('/alrevez',  function(req, res) {
            const { palabra } = req.body;
            res.status(200).json(palabra.split("").reverse().join(""))
        });

        this.app.post('/potencia',  function(req, res){
            const { number } = req.body;
            res.json({
                Potencia: number*number*number 
            });
        });
		
        this.app.post('/raiz', (req, resp)=>{
            let {num} = req.body;
            resp.json({
                raiz:Math.cbrt(num)
            })
        })

        this.app.post("/multi", function (req, res) {
            let {num, num2} = req.body;
            res.json({
                multiplicacion: num*num2
            });
        })

        this.app.post('/division', (req, resp)=>{
            let {num,num2} = req.body;
            if(num2 == 0){
                resp.json({
                    Division: "No es posible la division entre cero"
                })
            }
            resp.json({
                Division: num/num2
            })
        })

        this.app.post('/upper', (req, resp)=>{
            let {text} = req.body;
            resp.json({
                Upper: text.toUpperCase()
            })
        })

        this.app.post("/areacirculo", function (req, res) {
            let {radio} = req.body;
            res.json({
                area: (3.14*radio*radio) 
            });
        })
            //inicio logartimo
        this.app.post('/logaritmo',  function(req, res){
            const { number } = req.body;
            res.json({
                logaritmo: Math.log(number) 
            });
        });

        this.app.post('/inverso',  function(req, res) {
            const { numero } = req.body;
            res.status(200).json(1/numero)
        });

        this.app.post('/dual',  function(req, resp) {
            let {palabra} = req.body
            let duplicado = palabra+palabra
            resp.json({
                nuevapalabra:duplicado
            })
        });

    }

    listen(port) {
        this.server.listen(port, () => {
            console.log('running on ', port);
        });
    }
}
//comentario para mia aaux preferido uwu 
module.exports = Server;