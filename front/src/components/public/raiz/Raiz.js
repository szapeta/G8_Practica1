import axios from 'axios';
import React, { useState } from 'react'
import { types } from '../../../types/Types';

export const Raiz = () => {

    //estados para guardar las variables
    const [num, setNum] = useState("");
    const [resultado, setResultado] = useState("")

    //funcion para llamar a los endpoint
    const handlecalcular = async(e)=>{
        e.preventDefault()

        //types.apiurl = http://localhost:300 <-- configurado en el archivo /types/Types.js
        axios.post(types.apiurl+"raiz",{
            num
        }).then(result=>{

            //lectura del valor que devuelve el endpoin
            let valData = result.data;
            let valRaiz = valData.raiz;
            
            //se guarda el valor en el estado se usa: set
            setResultado(valRaiz);
        })
    }

  return (
    <div className="container mt-5 pb-5">
      <h1>Raiz</h1>
      <hr />
      <form autoComplete="off">
        <div className="form-group">
          <label>Numero</label>
          <input
            type="ingrese un valor"
            autoComplete="off"
            className="form-control"
            id="num"
            onChange={(event) => {
              setNum(event.target.value);
            }}
          />
        </div>
        <button className="btn btn-outline-primary" onClick={handlecalcular}>
          Calcular
        </button>
        <br/><br/>
        <div className="form-group">
          <label>Resultado:</label>
          <label className="form-control">{resultado}</label>
        </div>
      </form>
    </div>
  )
}
