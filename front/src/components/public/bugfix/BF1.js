import axios from 'axios';
import React, { useState } from 'react'
import { types } from '../../../types/Types';

export const BF1 = () => {

  //estados para guardar las variables
  const [text, setText] = useState("");
  const [resultado, setResultado] = useState("")

  //funcion para llamar a los endpoint
  const handlecalcular = async(e)=>{
      e.preventDefault()

      //types.apiurl = http://localhost:300 <-- configurado en el archivo /types/Types.js
      axios.post(types.apiurl+"upper",{
          text
      }).then(result=>{

          console.log(result)
          //lectura del valor que devuelve el endpoin
          let valData = result.data;
          let valRaiz = valData.Upper;
          
          //se guarda el valor en el estado se usa: set
          setResultado(valRaiz);
      })
  }

  return (
    <div className="container mt-5 pb-5">
      <h1>Upper</h1>
      <hr />
      <form autoComplete="off">
        <div className="form-group">
          <label>Texto:</label>
          <input
            type="ingrese un texto"
            autoComplete="off"
            className="form-control"
            id="text"
            onChange={(event) => {
              setText(event.target.value);
            }}
          />
        </div>
        <button className="btn btn-outline-primary" onClick={handlecalcular}>
          Transformar
        </button>
        <br/><br/>
        <div className="form-group">
          <label>Resultado:</label>
          <label className="form-control">{resultado}</label>
        </div>
      </form>
    </div>
  )
}
