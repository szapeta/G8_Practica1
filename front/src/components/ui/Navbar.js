import React from "react";
import { Link, NavLink } from "react-router-dom";

export const Navbar = () => {

    return (
      <div className="navbar-collapse">

        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link className="navbar-brand" to="/home">
                Practica1
            </Link>

            <div className="navbar-collapse">
            <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/parimpar"
                    >
                        Par o impar
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/fibonnacci"
                    >
                        Fibonnacci
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/alrevez"
                    >
                        Alrevez
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/potencia"
                    >
                        Potencia
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/raiz"
                    >
                        Raiz
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/bugfix1"
                    >
                        bugfix1
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/bugfix2"
                    >
                        bugfix2
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/bugfix3"
                    >
                        bugfix3
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/bugfix4"
                    >
                        bugfix4
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/bugfix5"
                    >
                        bugfix5
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/multi"
                    >
                        Multiplicacion
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/divi"
                    >
                        Division
                    </NavLink>
                </div>
            </div>
        </nav>

      </div>
    );
};

export default Navbar;
