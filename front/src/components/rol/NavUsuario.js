import React from 'react'
import { Link, NavLink, useHistory } from 'react-router-dom'

export const NavUsuario = () => {
    return (
        <>
            <div className="navbar-collapse">
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/lectores"
                    >
                        Lectores
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/newturnos"
                    >
                        Turnos
                    </NavLink>
                </div>
                <div className="navbar-nav">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link pr-5"
                        exact
                        to="/actividades"
                    >
                        Actividades
                    </NavLink>
                </div>
                <div className="navbar-nav pr-5">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link"
                        exact
                        to="/asistencia"
                    >
                        Asistencia
                    </NavLink>
                </div>
                <div className="navbar-nav pr-5">
                    <NavLink
                        activeClassName="active"
                        className="nav-item nav-link"
                        exact
                        to="/avisosadm"
                    >
                        Avisos
                    </NavLink>
                </div>
            </div>
        </>
    )
}