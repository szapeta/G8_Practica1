export const types = {
    login: '[auth] login',
    logout: '[auth] logout',
    urlfiles: 'http://localhost:8080/uploads/',

    apiurl:'http://localhost:3000/',
    rolUsuario: '[auth] UsuarioRegistrado',
    rolInvitado: '[auth] UsuariSinRegistrado',

}