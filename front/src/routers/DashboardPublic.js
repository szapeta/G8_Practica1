import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Alrevez } from '../components/public/alrevez/Alrevez';
import { BF1 } from '../components/public/bugfix/BF1';
import { BF2 } from '../components/public/bugfix/BF2';
import { BF3 } from '../components/public/bugfix/BF3';
import { BF4 } from '../components/public/bugfix/BF4';
import { BF5 } from '../components/public/bugfix/BF5';
import { Div } from '../components/public/div/Div';
import { Fibonnacci } from '../components/public/fibonnacci/Fibonnacci';

import { Home } from "../components/public/home/Home";
import { Mult } from '../components/public/mult/Mult';
import { Parimpar } from '../components/public/parimpar/Parimpar';
import { Potencia } from '../components/public/potencia/Potencia';
import { Raiz } from '../components/public/raiz/Raiz';

import Footer from '../components/ui/Footer';
import Navbar from '../components/ui/Navbar';

export const DashboardPublic = () => {
  return (
    <>
    <Navbar/>
      <div className='container mt-2'>
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/parimpar" component={Parimpar} />
        <Route path="/fibonnacci" component={Fibonnacci} />
        <Route path="/alrevez" component={Alrevez} />
        <Route path="/potencia" component={Potencia} />
        <Route path="/raiz" component={Raiz} />
        <Route path="/bugfix1" component={BF1} />
        <Route path="/bugfix2" component={BF2} />
        <Route path="/bugfix3" component={BF3} />
        <Route path="/bugfix4" component={BF4} />
        <Route path="/bugfix5" component={BF5} />
        <Route path="/multi" component={Mult} />
        <Route path="/divi" component={Div} />
        <Route path="/" component={Home} />
      </Switch>
      </div>
      <Footer/>
    </>
  );
};
