import React, { useContext } from "react";
import { Redirect } from "react-router-dom";

import { BrowserRouter as Router, Switch } from "react-router-dom";

import { AuthContext } from "../auth/AuthContext";

import { DashboardPublic } from "./DashboardPublic";

import { PublicRoute } from "./PublicRoute";

export const AppRouter = () => {
    const { user } = useContext(AuthContext);

    return (
        <Router>
            <div>
                <Switch>
                    <PublicRoute
                        exact
                        path="/home"
                        component={DashboardPublic}
                        isAuthenticated={user.logged}
                    />

                    <PublicRoute
                        path="/"
                        component={DashboardPublic}
                        isAuthenticated={user.logged}
                    />

                </Switch>
            </div>
        </Router>
    );
};
